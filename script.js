class Users {
    /**
     *
     * @param {HTMLElement} el
     * @param {HTMLElement} addBtn
     * @param {HTMLElement} form
     */
    constructor(el, addBtn, form) {
        this.element = el
        this.list = []
        this.form = form
        this.addBtn = addBtn
        this.addBtn.addEventListener('click', () => {
            this.initForm('Ajouter un utilisateur', 'Ajouter')
            this.form.querySelector('#close').addEventListener('click', this.close)
            this.form.style.display = 'block'
            const formevent = (e) => {
                e.preventDefault()
                const result = {
                    id: this.list.length + 1,
                    name: this.form.querySelector('#nom').value,
                    mdp: this.form.querySelector('#mdp').value
                }
                this.list.push(result)
                this.form.style.display = 'none'
                this.render()
                this.form.removeEventListener('submit', formevent)
            }
            this.form.addEventListener('submit', formevent)
        })

        this.render()
    }
    

}
initPagination(info)


//Initialisation des elements tableau
function init(index = 0){
    let elem = document.getElementById("table")
    tbodyInit(elem)
    let list;
    list = info
    if(info.length > 5){
        list = [];
        for(let i = index; i < index + nbreParPage; i++){
            list.push(info[i])
        }
        console.log(list)
    }
    render() {
        this.element.innerHTML = this.constructDom()
        Array.from(document.querySelectorAll('.update-btn, .delete-btn')).forEach((item) => {
            item.addEventListener('click', (item) => {
                if (item.target.dataset.type === 'delete'){
                    this.remove(parseInt(item.target.dataset.id))
                } else {
                    this.initForm('Modifier un utilisateur', 'Modifier', `${this.list.find(value => value.id === parseInt(item.target.dataset.id)).name}`, `${this.list.find(value => value.id === parseInt(item.target.dataset.id)).mdp}`)
                    this.form.style.display = 'block'
                    const formevent = (e) => {
                        e.preventDefault()
                        console.log("submit")
                        const index = this.list.findIndex(value => value.id === parseInt(item.target.dataset.id))
                        this.list[index].mdp = this.form.querySelector('#mdp').value
                        this.list[index].name = this.form.querySelector('#nom').value
                        this.form.style.display = 'none'
                        this.render()
                        this.form.removeEventListener('submit', formevent)
                    }
                    this.form.addEventListener('submit', formevent)
                }
            }
            for(let i = newId-1; i < info.length; i++){
                info[i].id = info[i].id - 1
            }
            let child = document.querySelector(".body").lastChild
            child.parentNode.removeChild(child)
            initPagination(info)
            refresh()
        }
    }
}
attach()



//pagination systeme
function pagination(e, index){
    let i = ((index * nbreParPage) - 5) + 1
    // console.log(document.querySelector('tbody').children)
    console.log(e)
    refresh(i-1)
}

//modification du dom pour ajouter des elements sur la table
function addElmt(element, login, password, userId){
    let user = userId
    console.log(userId)
    if(userId > 5){
        while(userId > 5){
            userId = userId - nbreParPage
        }
        
    }
    let tbody = document.querySelector('tbody')
    let tr = document.createElement('tr')
    let td1 = document.createElement("td")
    let td2 = document.createElement("td")
    let td3 = document.createElement("td")
    let td4 = document.createElement("td")
    let a1 = document.createElement("a")
    let a2 = document.createElement("a")
    let a3 = document.createElement("a")

    td1.innerHTML = user
    td2.innerHTML = login
    td3.innerHTML = password
    
    a1.href="#"
    a1.setAttribute('class', "view btn btn-small btn-primary mr-1 ")
    a1.innerHTML = "View"
    a1.setAttribute('onclick', "view(e,"+(userId)+")")

    a2.href="#"
    a2.setAttribute('class', "edit btn btn-small btn-success mr-1")
    a2.innerHTML = "Modifier"
    a2.setAttribute('onclick', "edit(e,"+(userId)+")")

    a3.href="#"
    a3.setAttribute("class", "sup btn btn-small btn-danger mr-1")
    a3.innerHTML = "Supprimer"
    // a3.setAttribute('onclick', "del(e,"+(userId)+")")

    // td4.appendChild(a1)
    td4.appendChild(a2)
    td4.appendChild(a3)

    tr.appendChild(td1)
    tr.appendChild(td2)
    tr.appendChild(td3)
    tr.appendChild(td4)

    tbody.appendChild(tr)
}


document.getElementById("creer").addEventListener("click", (e) => {
    let login = document.querySelector("#loginNew")
    let password = document.querySelector("#passwordNew")
    let userId = info.length + 1
    
    if(login.value != "" && password.value != ""){
        info.push({
            id: parseInt(userId),
            login: login.value,
            password: password.value
        })
        if(!document.querySelector('.pagination')){
            initPagination(info)
        }else{
            document.querySelector('.pagination').remove()
            initPagination(info)
        }
        refresh()
        e.target.parentNode.parentNode.parentNode.style.display="none"
        document.querySelector(".clo").style.display="none"
        
        //appel de function d'ajout
        // addElmt(document.getElementById("table").firstElementChild, login.value, password.value, userId)
        //clear inputs
        login.value=""
        password.value = ""
    }else{
        
        alert("Veuillez remplir tous les champs!!")
    }
}, false)

document.getElementById("edit").addEventListener("click", (e) => {
    let login = document.querySelector("#loginEdit")
    let password = document.querySelector("#passwordEdit")
    let userId = document.querySelector("#userId").value
    if(login.value != "" && password.value != ""){
        for(let i = 0; i<info.length; i++){
            if(info[i].id == userId){
                info.splice(i,1, {
                    id: parseInt(userId),
                    login:login.value,
                    password:password.value
                })
                refresh()
                break;
            }
        }
        e.target.parentNode.parentNode.parentNode.style.display="none"
        document.querySelector(".clo").style.display="none"
    }
    initForm(title, actionText, nameText= '', mdpText='') {
        this.form.querySelector('#title').textContent = title
        this.form.querySelector('#action').textContent = actionText
        this.form.querySelector('#nom').value = nameText
        this.form.querySelector('#mdp').value = mdpText
    }
}
